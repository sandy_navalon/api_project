const apiConfig = {
    baseUrl: 'https://api.themoviedb.org/3',
    apiKey: '59a3e1cea37208365af5565d3a7a133d',
    originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,

}

export default apiConfig;